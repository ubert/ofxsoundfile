#include "SoundFile.h"

SoundFile::SoundFile() {
}

bool SoundFile::load(const string filename)
{
    ofLog() << "loading soundfile: " << filename;
    this->sound_file = SndfileHandle(filename.c_str());
    this->isLoaded = (bool) this->sound_file;
    return this->isLoaded;
}

void SoundFile::setBlockSize(size_t frames) {
    this->blocksize = frames;
}

size_t SoundFile::getSampleRate() {
    return sound_file.samplerate();
}

uint32_t SoundFile::getFrames() {
    return sound_file.frames();
}

uint8_t SoundFile::getChannels() {
    return sound_file.channels();
}

ofSoundBuffer SoundFile::getBlock(const size_t block) {

    float tempBuffer[this->sound_file.channels() * this->blocksize];

    ofSoundBuffer buffer;

    if (this->blocksize > 0) {
        sf_count_t frames_read = this->sound_file.readf(tempBuffer, this->blocksize);
        buffer.copyFrom(
            tempBuffer,
            frames_read,
            this->sound_file.channels(),
            this->sound_file.samplerate()
        );
    }
    return(buffer);
}

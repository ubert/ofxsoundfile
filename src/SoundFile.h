#include "ofMain.h"
#include <sndfile.hh>

class SoundFile : ofBaseApp {

    public:
        SoundFile();
        bool load(const string filename);
        void setBlockSize(size_t frames);
        size_t getSampleRate();
        uint32_t getFrames();
        uint8_t getChannels();

        ofSoundBuffer getBlock(size_t block);

        bool isLoaded = false;

    private:
        SndfileHandle sound_file;
        size_t blocksize = 0;
};
